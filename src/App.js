import './App.css';
import Header from "./Header";
import List from "./List";

function App() {
  return (
      <>
        <Header />
        <List />
      </>
  );
}

export default App;
