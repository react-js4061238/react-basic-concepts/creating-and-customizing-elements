import {createElement} from 'react';

export default function Header() {
    return createElement(
        'h1',
        {style: { color: '#999', fontSize: '19px'}},
        'Solar system planets');
}
